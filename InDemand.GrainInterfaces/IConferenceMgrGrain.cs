using Orleans;
using System;
using System.Threading.Tasks;

namespace InDemand.GrainInterfaces
{
    public interface IConferenceMgrGrain : IGrainWithIntegerKey
    {
        Task AddConference(Guid conferenceId);
        Task RemoveConference(Guid conferenceId);
        Task<ConferenceSummary[]> GetConferenceSummaries();
    }
}
