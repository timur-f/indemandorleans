﻿using System.Threading.Tasks;
using Orleans;

namespace InDemand.GrainInterfaces
{
    public interface ISkillSetRegistry : IGrain
    {
        Task Register(IInterpreter interpreter);
        Task<IUser> FindCandidateBySkillSet(SkillSet desiredSkills);
    }


    public interface IUser : IGrainWithStringKey
    {
        
    }

    public class SkillSet
    {
        public string Gender { get; set; }
        public string Language { get; set; }
        public string Skill { get; set; }
    }

    public interface IInterpreter : IUser
    {
        Task<SkillSet> GetSkillSet();
    }
}