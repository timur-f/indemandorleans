using InDemand.GrainInterfaces;
using Microsoft.AspNetCore.Mvc;
using Orleans;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InDemand.Web.Controllers
{
    public class DomainController : Controller
    {
        public async Task<ActionResult> ListParticipants()
        {
            var allParticipantSummaries = new List<ParticipantSummary>();

            for (int i = 0; i < 256; i++)
            {
                var participantMgrGrain = GrainClient.GrainFactory.GetGrain<IParticipantMgrGrain>(i);
                var participantSummaries = await participantMgrGrain.GetParticipantSummaries();
                allParticipantSummaries.AddRange(participantSummaries);
            }

            return Json(new { currentParticipants = allParticipantSummaries.ToArray() });
        }

        public async Task<ActionResult> ListConferences()
        {
            var allConferenceSummaries = new List<ConferenceSummary>();

            for (int i = 0; i < 256; i++)
            {
                var conferenceMgrGrain = GrainClient.GrainFactory.GetGrain<IConferenceMgrGrain>(i);
                var conferenceSummaries = await conferenceMgrGrain.GetConferenceSummaries();
                allConferenceSummaries.AddRange(conferenceSummaries);
            }

            return Json(new { currentConferences = allConferenceSummaries.ToArray() });
        }
    }
}
