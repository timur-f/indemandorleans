using System;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using Orleans;
using Orleans.Concurrency;
using InDemand.GrainInterfaces;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace InDemand.Grains
{
    [Reentrant]
    public class ParticipantMgrGrain : Grain, IParticipantMgrGrain
    {
        private MemoryCache participants;

        public override Task OnActivateAsync()
        {
            participants = new MemoryCache("participants");
            return base.OnActivateAsync();
        }

        public static int GetShardFromKey(string participantId)
        {
            SHA1 sha = SHA1.Create();
            ASCIIEncoding ascii = new ASCIIEncoding();
            var participantBytes = ascii.GetBytes(participantId);
            var hash = sha.ComputeHash(participantBytes);
;           return hash[0];
        }

        public Task AddParticipant(string participantId)
        {
            participants.Add(participantId, true, new DateTimeOffset(DateTime.UtcNow).AddHours(1));
            return TaskDone.Done;
        }

        public Task RemoveParticipant(string participantId)
        {
            participants.Remove(participantId);
            return TaskDone.Done;
        }

        public async Task<ParticipantSummary[]> GetParticipantSummaries()
        {
            var participantSummaryPromises = new List<Task<ParticipantSummary>>();

            foreach (var kvp in participants)
            {
                participantSummaryPromises.Add(GrainFactory.GetGrain<IParticipantGrain>(kvp.Key).GetParticipantSummary());
            }

            await Task.WhenAll(participantSummaryPromises);
            return participantSummaryPromises.Select(x => x.Result).ToArray();
        }
    }
}
